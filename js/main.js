
lbMain: {
    let nNumber0 = inputInteger(`Fibonacci Number (Fn) calculation. Enter the integer number (F0)`);
    if (nNumber0 == null) break lbMain;

    let nNumber1 = inputInteger(`Enter the integer number (F1)`);
    if (nNumber1 == null) break lbMain;

    let nIndex = inputInteger(`Enter the Fibonacci Number index (n)`);
    if (nIndex == null) break lbMain;

    alert(`Fibonacci Number (F${nIndex}) = ${calcFibonacci(nNumber0, nNumber1, nIndex)}`);
}



function inputInteger(sMsg) {
    let sNum = "";
    let nNum;

    do {
        sNum = prompt(sMsg, sNum);
        if (sNum === null) return null;
        nNum = +sNum;
    } while (sNum === "" || Number.isNaN(nNum) || !Number.isInteger(nNum));

    return nNum;
}



function calcFibonacci(nNum0, nNum1, i) {
    if (i === 0) return nNum0;
    if (i === 1) return nNum1;

    let nSign = Math.sign(i);
    return nSign * calcFibonacci(nNum0, nNum1, i - nSign) + calcFibonacci(nNum0, nNum1, i - 2 * nSign);
}
